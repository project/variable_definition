<?php

/**
 * Implements hook_variable_info
 */
function variable_definition_variable_info() {
  $entities = entity_load('variable_definition');
  $info = array();
  // make arrays out of our objects
  if($entities) {
    foreach($entities as $entity) {
      $item = (array)$entity;
      // the 'vargroup' key is a hack - see variable_definition.install
      $item['group'] = $item['vargroup'];
      unset($item['vargroup']);

      $info[$entity->name] = $item;
    }
  }
  return $info;
}

/**
 * Implements hook_variable_group_info
 */
function variable_definition_variable_group_info() {
  $entities = entity_load('variable_definition_group');
  $info = array();
  // make arrays out of our objects
  if($entities) {
    foreach($entities as $entity) {
      $item = (array)$entity;
      $info[$entity->name] = $item;
    }
  }
  return $info;
}
